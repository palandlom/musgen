# my-project

(https://docs.moodle.org/310/en/Upload_users)

пример вх файла
(https://gitlab.com/palandlom/moodlakogen/-/tree/devjava/moodlakogen-java/src/test/resources)

Во входном файле - csv формат (prep):
```csv
Бакулин Александр Геннадьевич
Панфилов Михаил Михайлович  realemail@mail.com
Подлубний Андрей Андреевич  realemail@mail.com  non-generated-password
Ганнибал Андрей Преподов    realemail@mail.com  non-generated-password  prep
Гуторов Павел Дмитриевич    realemail@mail.com  NULL                    prep
Лутошкин Игорь Олегович     NULL                NULL                    prep
```

Выходной файл - csv формат:
```csv
username;firstname;lastname;email;password;country;lang;cohort1
zemcovskiiav;Александр;Земцовский;zemcovskii3420@mail.ru;zemcovskii2941;ru;ru;Преподаватели
shabalinaov;Ольга;Шабалина;shabalina3223@mail.ru;shabalina2154;ru;ru;Преподаватели
sergiyanskiiev;Евгений;Сергиянский;sergiyanskii3393@mail.ru;sergiyanskii3106;ru;ru;Преподаватели
```

Требования
- пароль с большой буквы,
- возможность формирования файлов с кодировкой UTF-8,
- возможность, при наличии e-mail, формировать данные для учеток без создания фейкового e-mail.
- возможность (исключительно, чтобы создавать учетки для преподавателей) внесение в поле ИМЯ - имени и отчества.


Интерейсы:
RawUserFileLoader -> Список RawUserDTO
Список RawUserDTO -> MoodleUserGenerator -> Список MooldeUserDTO
Список MooldeUserDTO -> UserFileWriter -> путь к выходному файлу


DTO
(https://ru.wikipedia.org/wiki/DTO)

`java -jar ./target/musgen.jar `


