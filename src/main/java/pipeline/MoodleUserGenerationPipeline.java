package pipeline;

import processors.MoodleUserGenerator;
import processors.RawUserFileLoader;
import processors.UserFileWriter;

import java.io.File;
import java.io.FileNotFoundException;

public class MoodleUserGenerationPipeline {

    private RawUserFileLoader loader;
    private MoodleUserGenerator generator;
    private UserFileWriter writer;
    private String inFilePath;
    private String outFilePath;

    public static MoodleUserGenerationPipeline.Builder getBuilder() {
        return new MoodleUserGenerationPipeline().new Builder();
    }

    public String Run() {
        return writer.writeMoodleUsers(this.outFilePath,
                generator.generateMoodleUsers(
                        loader.getRawUsers(this.inFilePath)));
    }

    /**
     * https://habr.com/ru/post/244521/
     */
    public class Builder {

        private Builder() {
            // private constructor
        }

        public Builder setInFilePath(String inFilePath) {
            MoodleUserGenerationPipeline.this.inFilePath = inFilePath;
            return this;
        }

        public Builder setOutFilePath(String outFilePath) {
            MoodleUserGenerationPipeline.this.inFilePath = inFilePath;
            return this;
        }

        public Builder setRawUserFileLoader(RawUserFileLoader loader) {
            MoodleUserGenerationPipeline.this.loader = loader;
            return this;
        }

        public Builder setMoodleUserGenerator(MoodleUserGenerator generator) {
            MoodleUserGenerationPipeline.this.generator = generator;
            return this;
        }

        public Builder setUserFileWriter(UserFileWriter writer) {
            MoodleUserGenerationPipeline.this.writer = writer;
            return this;
        }

        public MoodleUserGenerationPipeline build(){

            if (MoodleUserGenerationPipeline.this.generator==null) {
                System.out.println("Can't create pipeline - generator is not set.");
                return null;
            }

            if (MoodleUserGenerationPipeline.this.loader==null) {
                System.out.println("Can't create pipeline - loader is not set.");
                return null;
            }

            if (MoodleUserGenerationPipeline.this.writer==null) {
                System.out.println("Can't create pipeline - writer is not set.");
                return null;
            }

            return MoodleUserGenerationPipeline.this;
        }


    }
}
