package implementations;

/**
 * Выбрасывается при неправильных вхожных данных.
 */
public class BadRawValue extends Exception {

    /**
     *
     * @param s имя неправильного значения.
     */
    public BadRawValue(String s) {
        super(String.format("Bad raw value [%s] ",s));
    }
}
