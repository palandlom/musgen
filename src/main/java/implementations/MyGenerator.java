package implementations;

import models.RawUserDTO;
import processors.MoodleUserGenerator;

public class MyGenerator implements MoodleUserGenerator {

    public static final String GOOD_NAME = "good name";
    public static final String BAD_NAME = "bad name";

    @Override
    public String generateUsername(RawUserDTO rawUserDTO) {
        if (rawUserDTO == null ||
                (rawUserDTO.getFirstname()==null || rawUserDTO.getFirstname().trim().length() == 0 &&
                        rawUserDTO.getLastname()==null || rawUserDTO.getLastname().trim().length() == 0)) {
            return BAD_NAME;
        } else {
            return GOOD_NAME;
        }
    }

    @Override
    public String generateEmail(RawUserDTO rawUserDTO) {
        return null;
    }

    @Override
    public String generatePassword(RawUserDTO rawUserDTO) {
        return null;
    }

    @Override
    public RawUserDTO correctBadRawUserDTO(RawUserDTO rawUserDTO) {
        return null;
    }
}
