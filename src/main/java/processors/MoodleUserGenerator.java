package processors;

import models.MooldeUserDTO;
import models.RawUserDTO;

import java.util.List;
import java.util.stream.Collectors;

public interface MoodleUserGenerator {

    String generateUsername (RawUserDTO rawUserDTO);

    String generateEmail (RawUserDTO rawUserDTO);

    String generatePassword (RawUserDTO rawUserDTO);

    RawUserDTO correctBadRawUserDTO(RawUserDTO rawUserDTO);

    default List<MooldeUserDTO> generateMoodleUsers(List<RawUserDTO> rawUserDTOs){
        return rawUserDTOs.stream()
                .map(dto -> correctBadRawUserDTO(dto))
                .map(corrDto -> {
            MooldeUserDTO mooldeUserDTO = new MooldeUserDTO();
            mooldeUserDTO.setFirstname(corrDto.getFirstname());
            mooldeUserDTO.setMiddleName(corrDto.getMiddleName());
            mooldeUserDTO.setLastname(corrDto.getLastname());
            mooldeUserDTO.setCohort(corrDto.getCohort());
            mooldeUserDTO.setEmail(generateEmail(corrDto));
            mooldeUserDTO.setPassword(generatePassword(corrDto));
            mooldeUserDTO.setUsername(generateUsername(corrDto));
            return mooldeUserDTO;
        }).collect(Collectors.toList());
    }


}
