package processors;

import models.RawUserDTO;

import java.util.List;

public interface RawUserFileLoader {

    List<RawUserDTO> getRawUsers(String rawUserFilePath);
}
