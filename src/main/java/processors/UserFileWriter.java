package processors;

import models.MooldeUserDTO;
import models.RawUserDTO;

import java.util.List;

public interface UserFileWriter {


    String writeMoodleUsers(String moodleUserFilePath, List<MooldeUserDTO> mooldeUserDTOs);
}
