package main;

import implementations.MyGenerator;
import pipeline.MoodleUserGenerationPipeline;
import processors.MoodleUserGenerator;

public class Run {
    public static void main(String[] args) {

        MoodleUserGenerator generator = new MyGenerator();

        MoodleUserGenerationPipeline pipeline =
                MoodleUserGenerationPipeline.getBuilder()
                .setInFilePath("/in/file.csv")
                .setOutFilePath("/out/file.csv")
                .setMoodleUserGenerator(generator)
//                .setRawUserFileLoader()
//                .setUserFileWriter()
        .build();

        String mes = pipeline.Run();
        System.out.println(mes);
    }
}
