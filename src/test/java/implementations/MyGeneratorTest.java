package implementations;

import models.RawUserDTO;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import processors.MoodleUserGenerator;

public class MyGeneratorTest {

    private MoodleUserGenerator generator;

    private RawUserDTO goodRawUserDto;
    private RawUserDTO badRawUserDto;

    @Before
    public void setUp() throws Exception {
        this.generator = new MyGenerator();
        goodRawUserDto = getGoodRawDto();
        badRawUserDto = getBadRawDto();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void generateUsername() {
        Assert.assertEquals(MyGenerator.BAD_NAME, this.generator.generateUsername(badRawUserDto));
        Assert.assertEquals(MyGenerator.GOOD_NAME, this.generator.generateUsername(goodRawUserDto));
    }

    private static RawUserDTO getGoodRawDto()
    {
        RawUserDTO userDTO = new RawUserDTO();
        userDTO.setCohort("teachers");
        userDTO.setFirstname("Вася");
        userDTO.setMiddleName("Иванович");
        userDTO.setLastname("Рогов");
        return userDTO;
    }

    private static RawUserDTO getBadRawDto()
    {
        RawUserDTO userDTO = new RawUserDTO();
        return userDTO;
    }

}